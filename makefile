# set src to the name of the main file
SRC := CV_FR_Olaya.tex CV_EN_Olaya.tex CV_FR_Olaya_ING.tex
TARGETS := $(patsubst %.tex,%.pdf,$(SRC))
DIR=.

# Use this line, if you want to use latex command
TEX = pdflatex 
FLAGS = --shell-escape -synctex=1 -interaction=nonstopmode --src-specials -output-directory=$(DIR)
BIBTEX = biber

all : $(TARGETS)

$(TARGETS) : %.pdf : %.tex 
	$(TEX) $(FLAGS) $<
	$(BIBTEX) $(DIR)/$(basename $<)
	$(TEX) $(FLAGS) $<
	$(TEX) $(FLAGS) $<

clean :
	cp $(DIR)/*.pdf ./
	rm -f $(DIR)/*.*

	
	
